const Influx = require("influx");
const express = require("express");
const response = require("./api_response");
const path = require("path");
const bodyParser = require("body-parser");
require("./db_handlers/db_mongo_conn");
require("./db_handlers/seed");
// schdeduler handler
// require("./utils/alerts-scheduler");

const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);
var cors = require("cors");
var createError = require("http-errors");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var usersRouter = require("./routes/user");
var chartsRouter = require("./routes/chart");
var deviceRouter = require("./routes/device");
var alertRouter = require("./routes/alert");
var deviceAllocation = require("./routes/deviceAllocation");
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");
app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/users", usersRouter);
app.use("/charts", chartsRouter);
app.use("/devices", deviceRouter);
app.use("/alerts", alertRouter);
app.use("/deviceAllocations", deviceAllocation);

app.use(bodyParser.json());
//app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(express.static(path.join(__dirname, "public")));
//app.set("port", 3000);
app.set("port", process.env.PORT);

io.on("connection", (socket) => {
  console.log("Client connected");
  dataUpdate(socket);
});
http.listen(3002, () => {
  console.log("Listening on port 3002");
});

function dataUpdate(socket) {
  influx
    .query(
      `
  select E2 from mqtt_consumer
  where topic =~ /(?i)(energymeter)/
`
    )
    .then((results) => {
      socket.emit("dataupdate", results);
    });
  setTimeout(() => {
    dataUpdate(socket);
  }, 5000);
}
const influx = new Influx.InfluxDB({
  host: "18.221.38.143",
  username: "admin",
  password: "admin",
  port: 8086,
  database: "telegraf",
});

influx
  .getDatabaseNames()
  .then((names) => {
    if (!names.includes("ocean_tides")) {
      return influx.createDatabase("ocean_tides");
    }
  })
  .then(() => {
    app.listen(app.get("port"), () => {
      console.log(`Listening on ${app.get("port")}.`);
    });
  })
  .catch((error) =>
    console.log({
      error,
    })
  );

app.get("/getalldatabases", async (request, response) => {
  var databases = await influx.getDatabaseNames();
  let data = [];
  for (let i = 0; i < databases.length; i++) {
    var measurements = await influx.getMeasurements(databases[i]);
    let payload = {
      databasename: databases[i],
      measurements: measurements,
    };
    data.push(payload);
  }
  return response.status(200).json(data);
  // .then(
  //   console.log(result) result => response.status(200).json(result))
  // .catch(error => response.status(500).json({
  //   error
  // }));
});
app.get("/measurements:db", (request, response) => {
  const { db } = request.params;
  influx
    .getMeasurements(db)
    .then((result) => response.status(200).json(result))
    .catch((error) =>
      response.status(500).json({
        error,
      })
    );
});
