FROM node:10-alpine 

WORKDIR /usr/iot/server
COPY package.json .
RUN npm install    
COPY . .

ENV PORT 7700
EXPOSE 7700

CMD ["npm","run","start"]