const generate = require('nanoid/generate');


function genUserID(req, res, next) {
  if (req.body.role && req.body.role == 'patient') {
    req.body.registrationNumber = genPatientUserID()
  } else if (req.body.role && req.body.role == 'doctor') {
    req.body.registrationNumber = genDoctorUserID()
  } else if (req.body.role && req.body.role == 'admin') {
    req.body.registrationNumber = genAdminUserID()
  } else {
    next("error")
  }
  next();
}

function genAdminUserID() {
  const prefix = "ADM-"
  const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  return prefix + generate(alphabet, 7); //=> "589LY2V"
}

function genDoctorUserID() {
  const prefix = "DOC-"
  const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  return prefix + generate(alphabet, 7); //=> "589LY2V"
}

function genPatientUserID() {
  const prefix = "PAT-"
  const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  return prefix + generate(alphabet, 7); //=> "589LY2V"
}


module.exports = {
  genUserID
}