var schedule = require("node-schedule");
const model = require("../models/alert.model");
const service = require("../services/alert.service");
const Influx = require("influx");
const nodemailer = require("nodemailer");

var j = schedule.scheduleJob("* * * * *", async function () {
  console.log("Alert shedule job started....");
  const influx = await new Influx.InfluxDB({
    host: "18.221.38.143",
    username: "admin",
    password: "admin",
    port: 8086,
    database: "telegraf",
  });
  const alerts = await getAllAlerts();
  for (let i = 0; i < alerts.length; i++) {
    if (alerts[i].device_id != null) {
      await influx
        .query(
          `select last(${alerts[i].parameter}) from mqtt_consumer where topic =~ /(?i)(${alerts[i].device_id.device_id})/`
        )
        .then((results) => {
          if (results.length > 0) {
            if (alerts[i].condition == "=") {
              if (results[0].last == alerts[i].value) {
                console.log("Condition Matched.");
                email(alerts[i], results[0].last);
              }
            } else if (alerts[i].condition == ">") {
              if (results[0].last > alerts[i].value) {
                console.log("Condition Matched.");
                email(alerts[i], results[0].last);
              }
            } else if (alerts[i].condition == "<") {
              if (results[0].last < alerts[i].value) {
                console.log("Condition Matched.");
                email(alerts[i], results[0].last);
              }
            } else if (alerts[i].condition == "<=") {
              if (results[0].last <= alerts[i].value) {
                console.log("Condition Matched.");
                email(alerts[i], results[0].last);
              }
            } else if (alerts[i].condition == ">=") {
              if (results[0].last >= alerts[i].value) {
                console.log("Condition Matched.");
                email(alerts[i], results[0].last);
              }
            }
          }
        });
    }
  }
});

async function getAllAlerts() {
  return model
    .find({ active: true })
    .sort({ timestamp: "desc" })
    .populate("device_id");
}
async function email(activeAlert, lastvalue) {
  let transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "iot.radicalgrowth@gmail.com", //email address to send from
      pass: "IoT@RadicalGrowth", //the actual password for that account
    },
  });
  let subject = "IOT Device Warning";
  let mailOptions;
  mailOptions = {
    from: `"Radical Growth Solutions" <iot.radicalgrowth@gmail.com>`,
    name: "Radical Growth Solutions SUPPORT",
    to: activeAlert.destination,
    subject: subject,
    html: `<!DOCTYPE html>
    <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>IOT Device Warning</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style type="text/css">
      @media screen {
        @font-face {
          font-family: 'Source Sans Pro';
          font-style: normal;
          font-weight: 400;
          src: local('Source Sans Pro Regular'), local('SourceSansPro-Regular'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/ODelI1aHBYDBqgeIAH2zlBM0YzuT7MdOe03otPbuUS0.woff) format('woff');
        }
    
        @font-face {
          font-family: 'Source Sans Pro';
          font-style: normal;
          font-weight: 700;
          src: local('Source Sans Pro Bold'), local('SourceSansPro-Bold'), url(https://fonts.gstatic.com/s/sourcesanspro/v10/toadOcfmlt9b38dHJxOBGFkQc6VGVFSmCnC_l7QZG60.woff) format('woff');
        }
      }
    
      /**
       * Avoid browser level font resizing.
       * 1. Windows Mobile
       * 2. iOS / OSX
       */
      body,
      table,
      td,
      a {
        -ms-text-size-adjust: 100%; /* 1 */
        -webkit-text-size-adjust: 100%; /* 2 */
      }
    
      /**
       * Remove extra space added to tables and cells in Outlook.
       */
      table,
      td {
        mso-table-rspace: 0pt;
        mso-table-lspace: 0pt;
      }
    
      /**
       * Better fluid images in Internet Explorer.
       */
      img {
        -ms-interpolation-mode: bicubic;
      }
    
      /**
       * Remove blue links for iOS devices.
       */
      a[x-apple-data-detectors] {
        font-family: inherit !important;
        font-size: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
        color: inherit !important;
        text-decoration: none !important;
      }
    
      /**
       * Fix centering issues in Android 4.4.
       */
      div[style*="margin: 16px 0;"] {
        margin: 0 !important;
      }
    
      body {
        width: 100% !important;
        height: 100% !important;
        padding: 0 !important;
        margin: 0 !important;
      }
    
      /**
       * Collapse table borders to avoid space between cells.
       */
      table {
        border-collapse: collapse !important;
      }
    
      a {
        color: #1a82e2;
      }
    
      img {
        height: auto;
        line-height: 100%;
        text-decoration: none;
        border: 0;
        outline: none;
      }
      </style>
    
    </head>
    <body style="background-color: #e9ecef;">
      <div class="preheader" style="display: none; max-width: 0; max-height: 0; overflow: hidden; font-size: 1px; line-height: 1px; color: #fff; opacity: 0;">
        IOT Device Warning.
      </div>
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td align="center" bgcolor="#e9ecef">      
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <tr>
                <td align="left" bgcolor="#ffffff" style="padding: 36px 24px 0; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; border-top: 3px solid #d4dadf;">
                  <h1 style="margin: 0; font-size: 32px; font-weight: 700; letter-spacing: -1px; line-height: 48px;">IOT Device Warning</h1>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td align="center" bgcolor="#e9ecef">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
              <tr>
                <td align="left" bgcolor="#ffffff" style="padding: 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 24px;">
                  <p style="margin: 0;">Device Id: ${activeAlert.device_id.device_id}</p>
                  <p style="margin: 0;">Parameter: ${activeAlert.parameter}</p>
                  <p style="margin: 0;">Last Value: ${lastvalue}</p>
                  <p style="margin: 0;">Condition: ${activeAlert.condition}</p>
                  <p style="margin: 0;">Condition Value: ${activeAlert.value}</p>
                </td>
              </tr>
           
            </table>
          </td>
        </tr>
    
        <tr>
          <td align="center" bgcolor="#e9ecef" style="padding: 24px;">
           
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
    
              <tr>
                <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">
                  <p style="margin: 0;">This is an automatically generated email – please do not reply to it.</p>
                </td>
              </tr>
              <tr>
                <td align="center" bgcolor="#e9ecef" style="padding: 12px 24px; font-family: 'Source Sans Pro', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 20px; color: #666;">
                  <p style="margin: 0;">Radical Growth Solutions</p>
                </td>
              </tr>
             
            </table>
            </td>
        </tr>
      </table>
    </body>
    </html>`,
  };
  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log("error is " + error);
        resolve(false);
      } else {
        console.log("Email sent: " + info.response);
        service.updateAlert(activeAlert._id, { active: false });
        resolve(true);
      }
    });
  });
}
