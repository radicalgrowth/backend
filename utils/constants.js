var constants = {
  PORT: process.env.PORT,
  DB_HOST: process.env.DB_HOST,
  DB_NAME: process.env.DB_NAME,
  DB_USER: process.env.DB_USER,
  DB_PASS: process.env.DB_PASS,

  JWT_SECRET: process.env.JWT_SECRET,

  UP_PP_PATH: process.env.UP_PP_PATH,
  RET_PP_PATH: process.env.RET_PP_PATH,
  NODEMAILER_EMAIL: process.env.NODEMAILER_EMAIL,
  NODEMAILER_EMAIL_PASSWORD: process.env.NODEMAILER_EMAIL_PASSWORD,
  NODEMAILER_USER_NAME: process.env.NODEMAILER_USER_NAME,

  STRINGS: {
    RISETECHADDRESS: 'RISETech, ICE2, College of Electrical & Mechanical Engineering, Peshawar Road, Rawalpindi, Pakistan'
  }
};

module.exports = constants;