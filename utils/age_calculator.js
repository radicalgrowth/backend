const moment = require('moment');
const age_calculator = async(req, res, next) => {
  try {
    let { dob } = req.body;
    if (dob) {
      let birthday = moment(dob);
      req.body.age = Math.abs(birthday.diff(moment(), 'years'));
    }
    return next()
  } catch (error) {
    console.log(error)
    return next({ name: 'ServerError', errmsg: 'Age Not Calculated' })
  }
}
module.exports = {
  age_calculator
}
//test