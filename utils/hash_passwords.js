const bcrypt = require('bcryptjs');
const hashUserPassword = async(req, res, next) => {
  try {
    let { password } = req.body;
    req.body.password = await bcrypt.hash(password, 8)
    return next()
  } catch (error) {
    return next({ name: 'ServerError', errmsg: 'Hash Password Not Generated' })
  }
}

module.exports = {
  hashUserPassword
}