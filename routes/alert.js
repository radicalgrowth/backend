var express = require("express");
var controller = require("../controllers/alert.controller");
var router = express.Router();

router.post("/", controller.createAlert);
router.get("/", controller.getAllAlerts);
router.delete("/:id", controller.deleteAlert);
router.put("/:id", controller.updateAlert);
router.get("/:id", controller.getAlertById);

module.exports = router;
