var express = require("express");
var controller = require("../controllers/deviceAllocation.controller");
var router = express.Router();

router.post("/", controller.createdeviceAllocation);
router.get("/", controller.getAlldeviceAllocations);
router.delete("/:id", controller.deletedeviceAllocation);
router.put("/:id", controller.updatedeviceAllocation);
router.get("/:id", controller.getdeviceAllocationById);
router.get("/getdevicesbyuserid/:userid", controller.getDevicesByUserId);

module.exports = router;
