var express = require("express");
var controller = require("../controllers/device.controller");
var router = express.Router();

router.post("/", controller.createDevice);
router.get("/", controller.getAllDevices);
router.get("/getMaleehaData", controller.getMaleehaData);
router.get("/getthingstackdevices", controller.getThingStackDevices);
router.get("/getthingstackfields", controller.getThingStackFeilds);
router.delete("/:id", controller.deleteDevice);
router.delete("/:userid", controller.deleteDeviceByUserId);
router.put("/:id", controller.updateDevice);
router.get("/:id", controller.getDeviceById);
router.get("/getdevicesbyuserid/:userid", controller.getDevicesByUserId);
router.post("/getdatabydeviceid", controller.getDeviceDataById);
router.get("/getparametersbydeviceid/:id", controller.getDeviceParametersById);
router.post("/check-deviceid", controller.checkDeviceAlreadyRegistered);
router.get(
  "/getlasrdatabydeviceid/:id/:field",
  controller.getDeviceLastDataById
);
module.exports = router;
