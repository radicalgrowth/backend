var express = require("express");
var controller = require("../controllers/user.controller");
const user_validators = require("../validators/user.validator");
const param_validator = require("../validators/param.validator");
const hash_pass = require("../utils/hash_passwords");
const age_util = require("../utils/age_calculator");
//const auth = require('../middlewares/auth')
const pp_upload_config = require("../middlewares/image-upload");
var router = express.Router();

router.post(
  "/",
  [hash_pass.hashUserPassword, age_util.age_calculator],
  controller.createUser
);
router.get("/", controller.getUsers);
router.delete("/:id", controller.deleteUser);
router.put("/:id", controller.editUser);
router.get("/:id", controller.getUserById);

router.post(
  "/:id/upload_pp",
  [pp_upload_config.upload.single("pp")],
  controller.uploadPP
);
router.post(
  "/login",
  [user_validators.loginUserBodyValidator],
  controller.loginUser
);
router.put(
  "/:id/change_password",
  [user_validators.changeUserPasswordValidator],
  controller.changePassword
);
// router.patch("/:id/change_password", controller.changePassword);

router.post(
  "/login/silent",
  [user_validators.silentLoginUserBodyValidator],
  controller.silentLoginUser
);
router.post("/:id/logout", controller.logOut);
router.post("/:id/logout/all", controller.logOutAll);
router.get("/getbyregid/:reg_id", controller.getEmployeeByRegId);
// router.post('/signup', [user_validators.signUpUserBodyValidator], controller.signUpUser);
// router.get('/user_requestactive/:active', controller.getUserRequests);
// router.get('/user_request/:id', controller.getUserRequestById);
// router.post('/user_request/:id/inactive', controller.inactiveUserRequest);
router.get("/check-email/:email", controller.checkEmailAlreadyRegistered);
///////////////
router.post("/forgot-password-mock", controller.sendEmailTemplate);
router.post("/forgot-password", controller.changePassworRequest);
router.post(
  "/change-pass-verification/:id",
  controller.changePassworWithVerificationLink
);

module.exports = router;
