var express = require('express');
var controller = require('../controllers/chart.controller')
var router = express.Router();

router.post('/',controller.createChart);
router.get('/',controller.getAllCharts);
router.delete('/:id', controller.deleteChart);
router.put('/:id',controller.updateChart);
router.get('/:id',controller.getChartById);

module.exports = router;
