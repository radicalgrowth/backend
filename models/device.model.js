var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var deviceSchema = new Schema({
  device_id: { type: String, required: true },
  user_id: { type: Schema.Types.ObjectId, ref: "User", require: true },
  location_latitude: { type: String, default: "0" },
  location_longitude: { type: String, default: "0" },
  fields: [{ type: String }],
  timestamp: {
    type: Date,
    default: Date.now,
  },
});
var Device = mongoose.model("Device", deviceSchema);
module.exports = Device;
