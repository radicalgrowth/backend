const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var userSchema = new Schema({
  //common fields
  // regNo: { type: String, required: true, unique: true },
  name: String,
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true, select: false },
  role: { type: String, enum: ["admin", "user", "other"], required: true },
  profile_picture: { type: String, default: "" },
  phoneNumber: {
    type: String,
    // required: true,
    default: "",
  },
  address: { type: String, default: "" },
  dob: {
    type: Date,
    default: Date.now,
    //required: true
  },
  age: { type: Number, default: 0 },
  gender: { type: String, enum: ["male", "female"], required: true },
  description: { type: String, default: "" },
  timestamp: { type: Date, default: Date.now },
  //authentication
  tokens: [
    {
      token: {
        type: String,
        // required: true
      },
    },
  ],
});
userSchema.methods.addProfilePicture = async function (path) {
  // Generate an auth token for the user
  const user = this;
  user.profile_picture = path;
  await user.save();
  return user;
};
userSchema.methods.generateAuthToken = async function () {
  // Generate an auth token for the user
  const user = this;
  const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET);
  user.tokens = user.tokens.concat({ token });
  await user.save();
  return token;
};
userSchema.methods.comparePassword = async function (password) {
  // Generate an auth token for the user
  console.log(password, this.password);
  const user = this;
  const isPasswordMatch = await bcrypt.compare(password, user.password);
  console.log(isPasswordMatch);
  if (!isPasswordMatch) {
    return false;
  }
  return isPasswordMatch;
};

userSchema.statics.findByCredentials = async (email, password) => {
  try {
    const user = await User.findOne({ email })
      .select("+password")
      .populate("assignedTo", "name")
      .populate("disorder", "name")
      .populate({ path: "configurations", select: "status" });
    // console.log("USER", user)
    if (!user) {
      throw new Error("user not found");
    }
    const isPasswordMatch = await bcrypt.compare(password, user.password);
    if (!isPasswordMatch) {
      // console.log(isPasswordMatch)
      throw new Error("Invalid login credentials");
    }
    return user;
  } catch (err) {
    console.log(err);
  }
};

// the schema is useless so far
// we need to create a model using it
var User = mongoose.model("User", userSchema);
// make this available to our users in our Node applications
module.exports = User;
