var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var chartSchema = new Schema({
  device_id: { type: Schema.Types.ObjectId, ref: "Device", require: true },
  parameters: [{ type: String, required: true }],
  chart_type: { type: String, required: true },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});
var Chart = mongoose.model("Charts", chartSchema);
module.exports = Chart;
