var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var deviceAllocationSchema = new Schema({
  user_id: { type: Schema.Types.ObjectId, ref: "User", require: true },
  devices: [{ type: String }],
});
var DeviceAllocation = mongoose.model(
  "DeviceAllocation",
  deviceAllocationSchema
);
module.exports = DeviceAllocation;
