var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var alertSchema = new Schema({
  alert_name: { type: String, required: true },
  device_id: { type: Schema.Types.ObjectId, ref: "Device", require: true },
  parameter: { type: String, default: "" },
  condition: { type: String },
  value: { type: String },
  action: { type: String },
  destination: { type: String },
  active: { type: Boolean, default: true },
  timestamp: {
    type: Date,
    default: Date.now,
  },
});
var Alert = mongoose.model("Alert", alertSchema);
module.exports = Alert;
