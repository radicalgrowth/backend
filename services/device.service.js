const device_model = require("../models/device.model");
const Influx = require("influx");
var moment = require("moment");

async function createDevice(payload) {
  // console.log({ data: payload });
  let device = new device_model(payload);
  let temp_device = device.save();
  device = await device_model
    .findById((await temp_device)._id)
    .populate("user_id")
    .sort({ timestamp: "desc" });
  const influx = await new Influx.InfluxDB({
    host: "18.221.38.143",
    username: "admin",
    password: "admin",
    port: 8086,
    database: "telegraf",
  });
  for (let j = 0; j < device.fields.length; j++) {
    if (device.fields[j] != null && device.fields[j] != "") {
      await influx
        .query(
          `select last(${device.fields[j]}) from mqtt_consumer where topic = '${device.device_id}'`
        )
        .then((results) => {
          if (results.length > 0) {
            device.fields[j] = {
              name: device.fields[j],
              last: results[0].last,
            };
          } else {
            device.fields[j] = {
              name: device.fields[j],
              last: 0,
            };
          }
        });
    }
  }
  return device;
}
async function getAllDevices() {
  const influx = await new Influx.InfluxDB({
    host: "18.221.38.143",
    username: "admin",
    password: "admin",
    port: 8086,
    database: "telegraf",
  });
  let devices = await device_model
    .find({})
    .sort({ timestamp: "desc" })
    .populate("user_id");
  for (let i = 0; i < devices.length; i++) {
    for (let j = 0; j < devices[i].fields.length; j++) {
      if (devices[i].fields[j] != null && devices[i].fields[j] != "") {
        await influx
          .query(
            `select last(${devices[i].fields[j]}) from mqtt_consumer where topic = '${devices[i].device_id}'`
          )
          .then((results) => {
            // console.log(results, "results");
            if (results.length > 0) {
              devices[i].fields[j] = {
                name: devices[i].fields[j],
                last: results[0].last,
              };
            } else {
              devices[i].fields[j] = {
                name: devices[i].fields[j],
                last: 0,
              };
            }
          });
      }
    }
  }
  return devices;
}

async function getMaleehaData() {
  const influx = await new Influx.InfluxDB({
    host: "52.14.121.20",
    username: "admin",
    password: "admin",
    port: 8086,
    database: "telegraf",
  });

  //   const deviceTopic = `v3/${data.end_device_ids.application_ids.application_id}/devices/${data.end_device_ids.device_id}/up`;

  const data = await influx.query(
    `SELECT * FROM telegraf.autogen.mqtt_consumer WHERE topic = 'v3/rgs-test/devices/rgs-test-02/up' AND time > '2021-10-04T11:59:00.546Z'`
  );
  // console.log(data);
  /* for (let i = 0; i < data.length; i++) {
    // var dateTime = new Date(data[i].time);
    data[i].time = new Date(data[i].time.setHours(data[i].time.getHours() + 5));
  } */
  // console.log(JSON.parse(data));
  return data;
}

async function getDevicesByUserId(userid) {
  const influx = await new Influx.InfluxDB({
    host: "52.14.121.20",
    username: "admin",
    password: "admin",
    port: 8086,
    database: "telegraf",
  });
  let devices = await device_model
    .find({ user_id: userid })
    .sort({ timestamp: "desc" })
    .populate("user_id");
  for (let i = 0; i < devices.length; i++) {
    for (let j = 0; j < devices[i].fields.length; j++) {
      if (devices[i].fields[j] != null && devices[i].fields[j] != "") {
        await influx
          .query(
            `select last(${devices[i].fields[j]}) from mqtt_consumer where topic ='${devices[i].device_id}'`
          )
          .then((results) => {
            if (results.length > 0) {
              devices[i].fields[j] = {
                name: devices[i].fields[j],
                last: results[0].last,
              };
            } else {
              devices[i].fields[j] = {
                name: devices[i].fields[j],
                last: 0,
              };
            }
          });
      }
    }
  }
  // console.log(devices);
  return devices;
}
async function deleteDevice(id) {
  return await device_model.findByIdAndDelete(id);
}

async function deleteDeviceByUserId(userId) {
  // console.log("in device service");
  try {
    return await device_model.findOneAndDelete({ user_id: userId });
  } catch (err) {
    console.log(err);
  }
}
async function updateDevice(id, body) {
  let device = await device_model
    .findByIdAndUpdate(id, body, { new: true })
    .populate("user_id");
  const influx = await new Influx.InfluxDB({
    host: "18.221.38.143",
    username: "admin",
    password: "admin",
    port: 8086,
    database: "telegraf",
  });
  // await influx.writePoints([
  //   {
  //     measurement: "mqtt_consumer",
  //     tags: { topic: "/" + device.device_id },
  //     fields: { test: 1 },
  //   },
  // ]);
  return device;
}
async function getDeviceById(id) {
  return await device_model
    .findById(id)
    .sort({ timestamp: "desc" })
    .populate("user_id");
}
async function getDeviceDataById(deviceid, field, time) {
  let Mydata = [];
  const influx = await new Influx.InfluxDB({
    host: "52.14.121.20",
    username: "admin",
    password: "admin",
    port: 8086,
    database: "telegraf",
  });
  // My code
  // select mean(uplink_message_decoded_payload_battery) from telegraf.autogen.mqtt_consumer where time > now() - '2021-06-23T13:32:46.000Z' AND topic='v3/rgs-test/devices/rgs-test-02/up' GROUP BY time(5m)

  if (
    field.includes(
      "MEAN(uplink_message_decoded_payload_gndmoist) AS uplink_message_decoded_payload_gndmoist"
    ) ||
    field.includes(
      "MEAN(uplink_message_decoded_payload_gndmoist2) AS uplink_message_decoded_payload_gndmoist2"
    )
  ) {
    field = field.replace(
      "MEAN(uplink_message_decoded_payload_gndmoist) AS uplink_message_decoded_payload_gndmoist",
      "MEAN(uplink_message_decoded_payload_converted_gndmoist) AS uplink_message_decoded_payload_gndmoist"
    );
  }

  if (
    field.includes(
      "MEAN(uplink_message_decoded_payload_gndmoist2) AS uplink_message_decoded_payload_gndmoist2"
    )
  ) {
    field = field.replace(
      "MEAN(uplink_message_decoded_payload_gndmoist2) AS uplink_message_decoded_payload_gndmoist2",
      "MEAN(uplink_message_decoded_payload_converted_gndmoist2) AS uplink_message_decoded_payload_gndmoist2"
    );
  }

  /* Mydata = await influx.query(
    `select ${field} from mqtt_consumer where time >= '${time}' AND topic = '${deviceid}' GROUP BY time(5m)`
  ); */

  // For testing
  Mydata = await influx.query(
    `select ${field} from mqtt_consumer where time >= '${time}' AND topic = '${deviceid}'`
  );

  /* for (let i = 0; i < Mydata.length; i++) {
    console.log(Mydata[i].time);
  } */
  // console.log({ length: Mydata.length });

  // Old one code
  /* if (field != null) {
      await influx
        .query(
          `select ${field} from mqtt_consumer where time > now() - ${time} AND topic ='${deviceid}' GROUP BY time(5m)`
        )
        .then((results) => {
          // console.log(results);
          Mydata = results;
        });
    } */

  return Mydata;
}
async function getDeviceLastDataById(deviceid, field) {
  try {
    Mydata = null;
    const influx = await new Influx.InfluxDB({
      host: "18.221.38.143",
      username: "admin",
      password: "admin",
      port: 8086,
      database: "telegraf",
    });
    await influx
      .query(
        `select ${field} from mqtt_consumer where topic ='${deviceid}' ORDER BY ASC LIMIT 1`
      )
      .then((results) => {
        Mydata = results;
      });
    return Mydata;
  } catch (error) {
    console.log(error);
  }
}
async function getDeviceParametersById(deviceid) {
  try {
    Mydata = null;
    const influx = await new Influx.InfluxDB({
      host: "52.14.121.20",
      username: "admin",
      password: "admin",
      port: 8086,
      database: "telegraf",
    });
    await influx
      .query(`SHOW FIELD KEYS ON telegraf FROM mqtt_consumer`)
      .then((results) => {
        Mydata = results;
      });
    var filtered_feilds = await Mydata.filter(function (val) {
      // return val.fieldKey.startsWith(deviceid);
      return val.fieldKey;
    });

    // My Code
    const device = await device_model.findById(deviceid);
    // My Code

    for (let i = 0; i < filtered_feilds.length; i++) {
      if (
        filtered_feilds[i].fieldKey !== null &&
        filtered_feilds[i].fieldKey !== ""
      ) {
        await influx
          .query(
            `select last(${filtered_feilds[i].fieldKey}) from mqtt_consumer where topic ='${device.device_id}'`
          )
          .then((results) => {
            if (results.length > 0) {
              filtered_feilds[i]["last"] = results[0].last;
            }
          });
      }
    }
    /* console.log({
      message: "data after last query",
      result: filtered_feilds,
    }); */
    return filtered_feilds;
  } catch (error) {
    console.log(error);
  }
}
async function checkDeviceAlreadyRegistered(deviceId) {
  try {
    const device = await device_model.findOne({ device_id: deviceId });
    if (!device) {
      return false;
    } else {
      return true;
    }
  } catch (error) {
    throw error;
  }
}
async function getThingsStackDevices() {
  try {
    const influx = await new Influx.InfluxDB({
      host: "52.14.121.20",
      username: "admin",
      password: "admin",
      port: 8086,
      database: "telegraf",
    });
    let data = [];
    await influx
      .query(`SHOW TAG VALUES ON "telegraf" WITH KEY = "topic"`)
      .then((results) => {
        // console.log(results);
        results.forEach((element) => {
          if (element.value.includes("v3/rgs-test/devices/")) {
            let value = element.value.replace("v3/rgs-test/devices/", "");
            let payload = {
              label: value,
              value: element.value,
            };
            data.push(payload);
          }
        });
      });
    return data;
  } catch (error) {
    console.log(error);
  }
}
async function getThingsStackFeilds() {
  try {
    const influx = await new Influx.InfluxDB({
      host: "52.14.121.20",
      username: "admin",
      password: "admin",
      port: 8086,
      database: "telegraf",
    });
    let data = [];
    await influx
      .query(`SHOW FIELD KEYS ON "telegraf" FROM "mqtt_consumer"`)
      .then((results) => {
        // console.log({ data: results });
        /* results.splice(
          results.findIndex(
            (el) =>
              el.fieldKey ===
              "uplink_message_decoded_payload_converted_gndmoist"
          ),
          1
        ); */
        results.splice(2, 4);
        results.forEach((element) => {
          let value, payload;
          if (
            element.fieldKey.includes("uplink_message_decoded_payload_") &&
            element.fieldType === "float"
          ) {
            value = element.fieldKey.replace(
              "uplink_message_decoded_payload_",
              ""
            );

            payload = {
              label: value,
              value: element.fieldKey,
            };
            // }
            data.push(payload);
          }
        });
      });
    return data;
  } catch (error) {
    console.log(error);
  }
}

module.exports = {
  createDevice,
  getAllDevices,
  getMaleehaData,
  deleteDevice,
  deleteDeviceByUserId,
  updateDevice,
  getDeviceById,
  checkDeviceAlreadyRegistered,
  getDeviceDataById,
  getDeviceParametersById,
  getDeviceLastDataById,
  getDevicesByUserId,
  getThingsStackDevices,
  getThingsStackFeilds,
};
