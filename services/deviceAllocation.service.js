const allocation_model = require("../models/deviceAllocation.model");
const device_model = require("../models/device.model");
const alert_model = require("../models/alert.model");
const chart_model = require("../models/charts.model");
const device_service = require("./device.service");

async function createdeviceAllocation(payload) {
  try {
    // create deviceAllocation
    let deviceAllocation = await allocation_model.create(payload);

    // Make fields for device entry
    const fields = [];
    for (const device of deviceAllocation.devices) {
      fields.push(
        await device_model
          .findOne({ device_id: device })
          .select({ fields: 1, _id: 1, device_id: 1 })
      );
    }

    // Save device by user id one by one
    for (const field of fields) {
      // console.log({ field, user: deviceAllocation.user_id });
      const device = new device_model({
        device_id: field.device_id,
        user_id: deviceAllocation.user_id,
        fields: field.fields,
      });
      await device.save();
    }

    return await this.getdeviceAllocationById(deviceAllocation._id);
  } catch (err) {
    console.log(err);
  }
}
async function getAlldeviceAllocations() {
  return allocation_model
    .find({})
    .sort({ timestamp: "desc" })
    .populate("user_id");
}
async function deletedeviceAllocation(id) {
  const devices = await allocation_model
    .findById(id)
    .select({ devices: 1, user_id: 1 });

  if (devices) {
    for (let i = 0; i < devices.devices.length; i++) {
      const deviceId = await device_model
        .findOne({ user_id: devices.user_id })
        .select({ _id: 1 });
      await alert_model.deleteOne({ device_id: deviceId });
      await chart_model.deleteOne({ device_id: deviceId });
      // console.log({ device: devices.devices[i], deviceId });

      await device_service.deleteDevice(deviceId);
    }
  }

  return await allocation_model.findByIdAndDelete(id);
}
async function updatedeviceAllocation(id, body) {
  let deviceAllocation = allocation_model.findByIdAndUpdate(id, body, {
    new: true,
  });
  return deviceAllocation;
}
async function getdeviceAllocationById(id) {
  return allocation_model
    .findById(id)
    .sort({ timestamp: "desc" })
    .populate("user_id");
}
async function getDevicesByUserId(userid) {
  try {
    let data = [];
    let devices = await allocation_model.findOne({ user_id: userid });
    // console.table(devices);
    devices.devices.forEach((element) => {
      if (element.includes("v3/rgs-test/devices/")) {
        let value = element.replace("v3/rgs-test/devices/", "");
        let payload = {
          label: value,
          value: element,
        };
        data.push(payload);
      }
    });
    return data;
  } catch (e) {
    return e;
  }
}

module.exports = {
  createdeviceAllocation,
  getAlldeviceAllocations,
  deletedeviceAllocation,
  updatedeviceAllocation,
  getdeviceAllocationById,
  getDevicesByUserId,
};
