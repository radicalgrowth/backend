const chart_model = require("../models/charts.model");
var moment = require("moment");

async function createChart(payload) {
  console.log({ payloadData: payload });
  // let chart = new chart_model(payload);
  // let temp_chart = chart.save();
  let temp_chart = await chart_model.create({
    device_id: payload.device_id,
    parameters: payload.parameters,
    chart_type: payload.chart_type,
  });
  return chart_model
    .findById((await temp_chart)._id)
    .populate("device_id")
    .sort({ timestamp: "desc" });
}
async function getAllCharts() {
  return chart_model.find({}).sort({ timestamp: "desc" }).populate("device_id");
}
async function deleteChart(id) {
  return chart_model.findByIdAndDelete(id);
}
async function updateChart(id, body) {
  let chart = chart_model.findByIdAndUpdate(id, body, { new: true });
  return chart;
}
async function getChartById(id) {
  return chart_model
    .findById(id)
    .sort({ timestamp: "desc" })
    .populate("device_id");
}

module.exports = {
  createChart,
  getAllCharts,
  deleteChart,
  updateChart,
  getChartById,
};
