const _ = require("lodash");
const bcrypt = require("bcryptjs");
const user_model = require("./../models/user.model");
//const user_request_model = require('../models/user_request.model')
const constants = require("../utils/constants");
const ret_pic_path = constants.RET_PP_PATH;

async function uploadPP(file, id) {
  let pic_path = `${ret_pic_path}/${id}/${file.filename}`;
  let user = await user_model.findById(id);
  if (!user) throw new Error("User Not Found");
  let upUser = await user.addProfilePicture(pic_path);
  // console.log(upUser);
  return this.getUserById(upUser._id);
}
async function createUser(user_payload) {
  try {
    let user = await new user_model(user_payload);
    return user.save();
  } catch (error) {
    throw error;
  }
}
async function silentLoginUser(user_id, token) {
  try {
    const user = await user_model
      .findOne({ _id: user_id })
      .populate("assignedTo", "name")
      .populate("disorder", "name")
      .populate({ path: "configurations", select: "status" });
    if (!user) {
      throw new Error("Login failed! Check authentication credentials");
    }
    let index = user.tokens.findIndex((tok) => tok.token == token);
    // console.log(index)
    if (index < 0) {
      throw new Error("Login failed! Check authentication credentials");
    }
    // const token = await user.generateAuthToken();
    let data = {
      user: user,
      token: user.tokens[index],
    };
    return data;
  } catch (error) {
    throw error;
  }
}
async function loginUser(email, password) {
  try {
    const user = await user_model.findByCredentials(email, password);

    if (!user) {
      next({
        name: "ServerError",
        errmsg: "Login failed! Check authentication credentials",
      });
    }
    const token = await user.generateAuthToken();
    let data = {
      user: user,
      token: token,
    };
    return data;
  } catch (error) {
    throw error;
  }
}
/* async function changePassword(id, current, new_password) {
  try {
    const user = await user_model.findById(id).select("+password");
    console.log(user.password);
    if (!user) {
      throw new Error("No User Found With This Id");
    }
    let isPasswordMatch = await user.comparePassword(current);
    console.log(isPasswordMatch);
    if (!isPasswordMatch) {
      throw new Error("Wrong Password");
    }
    let encrypted_pass = await bcrypt.hash(new_password, 8);
    let body = {
      password: encrypted_pass,
    };
    let data = user_model
      .findByIdAndUpdate(id, body, { new: true })
      .populate("assignedTo", "name")
      .populate("disorder", "name")
      .populate({ path: "configurations", select: "status" }); 
    return data;
  } catch (error) {
    throw error;
  }
} */
async function getUserById(id) {
  return user_model
    .findById(id)
    .lean()
    .populate("assignedTo", "name")
    .populate("disorder", "name")
    .populate({ path: "configurations", select: "status" });
}
async function getDoctorPatients(id) {
  return user_model.find({ assignedTo: id });
}
async function getUsers() {
  return user_model
    .find({})
    .lean()
    .populate("assignedTo", "name")
    .populate("disorder", "name")
    .populate({ path: "configurations", select: "status" });
}
async function updateUser(id, body) {
  let { password } = body;
  if (password) {
    body.password = await bcrypt.hash(password, 8);
  }
  let user = user_model
    .findByIdAndUpdate(id, body, { new: true })
    .populate("assignedTo", "name")
    .populate("disorder", "name")
    .populate({ path: "configurations", select: "status" });
  return user;
}

async function getUsersByRole(role) {
  return user_model
    .find({ role: role })
    .lean()
    .populate("assignedTo", "name")
    .populate("disorder", "name")
    .populate({ path: "configurations", select: "status" })
    .sort({ timestamp: -1 });
}
async function deleteUser(id) {
  // console.log(id, "id");
  return user_model.findByIdAndDelete(id);
}
async function logOutUser(id, token) {
  let user = await user_model.findById(id).select("+password");
  if (!user) throw new Error("User Not Found");
  user.tokens = user.tokens.filter((t) => {
    return t.token != token;
  });

  return this.createUser(user);
}
async function logOutAllUser(id) {
  let user = await user_model.findById(id).select("+password");
  if (!user) throw new Error("User Not Found");
  user.tokens.splice(0, user.tokens.length);

  return this.createUser(user);
}
async function getEmployeeByRegId(id) {
  return user_model.findOne({ regNo: id });
}
// async function signUpUser(payload) {
//   try {
//     let user_request = new user_request_model(payload);
//     return user_request.save()
//   } catch (error) {
//     throw error;
//   }
// }
// async function getUserRequests(active) {
//   try {
//     if (active) {
//       return user_request_model.find({ active: active });
//     }
//     return user_request_model.find({});
//   } catch (error) {
//     throw error;
//   }
// }
// async function getUserRequestById(id) {
//   try {
//     return user_request_model.findById(id);
//   } catch (error) {
//     throw error;
//   }
// }
// async function inactiveUserRequest(id) {
//   try {
//     return user_request_model.findByIdAndUpdate(id, { active: false }, { new: true });
//   } catch (error) {
//     throw error;
//   }
// }
async function changePasswordRequest(email) {
  try {
    email = email.toLowerCase();
    const user = await user_model.findOne({ email: email });
    if (!user) {
      throw new Error("No User Registered With This Email");
    }
    return user;
  } catch (error) {
    throw error;
  }
}
async function checkEmailAlreadyRegistered(email) {
  try {
    email = email.toLowerCase();
    const user = await user_model.findOne({ email: email });
    if (!user) {
      return false;
    } else {
      return true;
    }
  } catch (error) {
    throw error;
  }
}
async function changePassworWithVerificationLink(id, new_password) {
  try {
    const user = await user_model.findById(id).select("+password");
    if (!user) {
      throw new Error("No User Found With This Id");
    }
    let encrypted_pass = await bcrypt.hash(new_password, 8);
    let body = {
      password: encrypted_pass,
    };
    let data = user_model
      .findByIdAndUpdate(id, body, { new: true })
      .populate("assignedTo", "name")
      .populate("disorder", "name")
      .populate({ path: "configurations", select: "status" });
    return data;
  } catch (error) {
    throw error;
  }
}
async function changePassword(id, current, new_password) {
  try {
    const user = await user_model.findById(id).select("+password");

    if (!user) {
      throw new Error("No User Found With This Id");
    }

    let isPasswordMatch = await user.comparePassword(current);
    if (!isPasswordMatch) {
      throw new Error("Wrong Password");
    }
    let encrypted_pass = await bcrypt.hash(new_password, 8);
    let body = {
      password: encrypted_pass,
    };
    let data = user_model
      .findByIdAndUpdate(id, body, { new: true })
      .populate("assignedTo", "name")
      .populate("disorder", "name")
      .populate({ path: "configurations", select: "status" });
    return data;
  } catch (error) {
    throw error;
  }
}

/* async function changePassword(id, email, new_password) {
  const user = await user_model.findById(id).select("+password");

  if (!user) {
    throw new Error("No User Found With This Id");
  }

  // let isPasswordMatch = await user.comparePassword(current);
  // if (!isPasswordMatch) {
  //   throw new Error("Wrong Password");
  // } 
  let encrypted_pass = await bcrypt.hash(new_password, 8);
  let body = {
    password: encrypted_pass,
  };
  let data = await user_model
    .findByIdAndUpdate(id, body, { new: true })
    .populate("assignedTo", "name")
    .populate("disorder", "name")
    .populate({ path: "configurations", select: "status" });
  // console.log({ id, email, new_password, encrypted_pass, data });
  return data;
} */

module.exports = {
  createUser,
  uploadPP,
  // changePassword,
  getUserById,
  getDoctorPatients,
  getUsers,
  loginUser,
  silentLoginUser,
  updateUser,
  getUsersByRole,
  deleteUser,
  logOutUser,
  logOutAllUser,
  getEmployeeByRegId,
  // signUpUser,
  // getUserRequests,
  // getUserRequestById,
  // inactiveUserRequest,
  changePassword,
  changePassworWithVerificationLink,
  changePasswordRequest,
  checkEmailAlreadyRegistered,
};
