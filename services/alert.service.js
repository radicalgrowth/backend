const model = require("../models/alert.model");
var moment = require("moment");

async function createAlert(payload) {
  let alert = new model(payload);
  let temp_alert = alert.save();
  return model
    .findById((await temp_alert)._id)
    .sort({ timestamp: "desc" })
    .populate("device_id");
}
async function getAllAlerts() {
  return model.find({}).sort({ timestamp: "desc" }).populate("device_id");
}
async function deleteAlert(id) {
  return model.findByIdAndDelete(id);
}
async function updateAlert(id, body) {
  let alert = model.findByIdAndUpdate(id, body, { new: true });
  return alert;
}
async function getAlertById(id) {
  return model.findById(id).sort({ timestamp: "desc" }).populate("device_id");
}

module.exports = {
  createAlert,
  getAllAlerts,
  deleteAlert,
  updateAlert,
  getAlertById,
};
