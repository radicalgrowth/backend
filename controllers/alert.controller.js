const api_response = require("../utils/api_response");
const service = require("../services/alert.service");

async function createAlert(req, res, next) {
  try {
    let payload = req.body;
    let data = await service.createAlert(payload);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    next({ name: "ServerError", errmsg: error });
  }
}
async function getAllAlerts(req, res, next) {
  try {
    let data = await service.getAllAlerts();
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    return next({ name: "ServerError", errmsg: error.message });
  }
}
async function deleteAlert(req, res, next) {
  try {
    let { id } = req.params;
    let deletedData = await service.deleteAlert(id);
    res.status(api_response.code.OK).json(api_response.success(deletedData));
  } catch (error) {
    return next({ name: "ServerError", errmsg: error.message });
  }
}
async function updateAlert(req, res, next) {
  try {
    const { id } = req.params;
    const configPayload = req.body;
    let data = await service.updateAlert(id, configPayload);
    res.status(api_response.code.OK).json(api_response.success(data));
  } catch (error) {
    console.log(error);
    next(error);
  }
}
async function getAlertById(req, res, next) {
  try {
    const { id } = req.params;
    let data = await service.getAlertById(id);
    if (data == null) {
      next({ name: "ServerError", errmsg: "Object Not Found" });
    }
    let response = api_response.success(data);
    res.json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: "Object Not Found" });
  }
}

module.exports = {
  createAlert,
  getAllAlerts,
  deleteAlert,
  updateAlert,
  getAlertById,
};
