const api_response = require("../utils/api_response");
const service = require("../services/device.service");

async function createDevice(req, res, next) {
  try {
    let payload = req.body;
    let data = await service.createDevice(payload);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    next({ name: "ServerError", errmsg: error });
  }
}

async function getAllDevices(req, res, next) {
  try {
    let data = await service.getAllDevices();
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    return next({ name: "ServerError", errmsg: error.message });
  }
}

async function getMaleehaData(req, res, next) {
  try {
    let data = await service.getMaleehaData();
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    return next({ name: "ServerError", errmsg: error.message });
  }
}

async function getThingStackDevices(req, res, next) {
  try {
    let data = await service.getThingsStackDevices();
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    return next({ name: "ServerError", errmsg: error.message });
  }
}

async function getThingStackFeilds(req, res, next) {
  try {
    let data = await service.getThingsStackFeilds();
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    return next({ name: "ServerError", errmsg: error.message });
  }
}

async function deleteDevice(req, res, next) {
  try {
    let { id } = req.params;
    let deletedData = await service.deleteDevice(id);
    res.status(api_response.code.OK).json(api_response.success(deletedData));
  } catch (error) {
    return next({ name: "ServerError", errmsg: error.message });
  }
}

async function deleteDeviceByUserId(req, res, next) {
  try {
    let { userid } = req.params;
    let deletedData = await service.deleteDeviceByUserId(userid);
    res.status(api_response.code.OK).json(api_response.success(deletedData));
  } catch (error) {
    return next({ name: "ServerError", errmsg: error.message });
  }
}

async function updateDevice(req, res, next) {
  try {
    const { id } = req.params;
    const configPayload = req.body;
    let data = await service.updateDevice(id, configPayload);
    res.status(api_response.code.OK).json(api_response.success(data));
  } catch (error) {
    console.log(error);
    next(error);
  }
}

async function getDeviceById(req, res, next) {
  try {
    const { id } = req.params;
    let data = await service.getDeviceById(id);
    if (data == null) {
      next({ name: "ServerError", errmsg: "Object Not Found" });
    }
    let response = api_response.success(data);
    res.json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: "Object Not Found" });
  }
}

async function getDevicesByUserId(req, res, next) {
  try {
    const { userid } = req.params;
    let data = await service.getDevicesByUserId(userid);
    if (data == null) {
      next({ name: "ServerError", errmsg: "Object Not Found" });
    }
    let response = api_response.success(data);
    res.json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: "Object Not Found" });
  }
}

async function getDeviceDataById(req, res, next) {
  try {
    const payload = req.body;

    const data = await service.getDeviceDataById(
      payload.id,
      payload.field,
      payload.time
    );
    // console.log({ data: data });
    if (!data) {
      next({ name: "ServerError", errmsg: "Object Not Found" });
    }
    let response = api_response.success(data);
    res.json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error });
  }
}

async function getDeviceLastDataById(req, res, next) {
  try {
    const { id, field } = req.params;
    let data = await service.getDeviceLastDataById(id, field);
    if (data == null) {
      next({ name: "ServerError", errmsg: "Object Not Found" });
    }
    let response = api_response.success(data);
    res.json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: "Object Not Found" });
  }
}

async function getDeviceParametersById(req, res, next) {
  try {
    const { id } = req.params;
    let data = await service.getDeviceParametersById(id);
    if (data == null) {
      next({ name: "ServerError", errmsg: "Object Not Found" });
    }
    let response = api_response.success(data);
    res.json(response.last);
  } catch (error) {
    next({ name: "ServerError", errmsg: "Object Not Found" });
  }
}

async function checkDeviceAlreadyRegistered(req, res, next) {
  try {
    let deviceId = req.body.deviceid;
    // console.log(deviceId, "deviceId");
    let data = await service.checkDeviceAlreadyRegistered(deviceId);
    let response;
    if (data == false) {
      response = api_response.success("Device Not registered !!!");
      res.status(api_response.code.OK).json(response);
    } else {
      response = api_response.success(null);
      res.status(api_response.code.OK).json(response);
    }
  } catch (error) {
    next({
      name: "ServerError",
      errmsg: error,
    });
  }
}

module.exports = {
  createDevice,
  getAllDevices,
  getMaleehaData,
  deleteDevice,
  deleteDeviceByUserId,
  updateDevice,
  getDeviceById,
  checkDeviceAlreadyRegistered,
  getDeviceDataById,
  getDeviceParametersById,
  getDeviceLastDataById,
  getDevicesByUserId,
  getThingStackDevices,
  getThingStackFeilds,
};
