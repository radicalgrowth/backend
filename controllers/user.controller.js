const api_response = require("../utils/api_response");
const user_service = require("../services/user.service");
const email_handler = require("../utils/email_handler").sendResetPasswordLink;

async function uploadPP(req, res, next) {
  let { id } = req.params;
  try {
    let data = await user_service.uploadPP(req.file, id);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error });
  }
}

//HUSSNIAN
async function createUser(req, res, next) {
  try {
    let user_payload = req.body;
    let user = null;
    user = await user_service.createUser(user_payload);
    //user = await user_service.getUserById(user._id);
    let response = api_response.success(user);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error });
  }
}

async function loginUser(req, res, next) {
  try {
    const { email, password } = req.body;
    let data = await user_service.loginUser(email, password);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error.stack });
  }
}

/* async function changePassword(req, res, next) {
  try {
    const { id } = req.params;
    const { current, new_password } = req.body;
    let data = await user_service.changePassword(id, current, new_password);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error });
  }
} */

async function silentLoginUser(req, res, next) {
  try {
    const { user_id, token } = req.body;
    let data = await user_service.silentLoginUser(user_id, token);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error.stack });
  }
}

//HUSSNIAN
async function getUserById(req, res, next) {
  try {
    const { id } = req.params;
    let data = await user_service.getUserById(id);
    if (data != null && data.role == "doctor") {
      let assignedPatients = await user_service.getDoctorPatients(id);
      data.assignedPatients = assignedPatients;
    }
    let response = api_response.success(data);
    return res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    next({ name: "ServerError", errmsg: error });
  }
}

//HUSSNIAN
async function getUsers(req, res, next) {
  try {
    let { role } = req.query;
    //console.log(role);
    let data = null;
    if (role) {
      data = await user_service.getUsersByRole(role);
    } else {
      data = await user_service.getUsers();
    }
    for (let i = 0; i < data.length; i++) {
      const doctor = data[i];
      let assignedPatients = await user_service.getDoctorPatients(doctor._id);
      data[i].assignedPatients = assignedPatients;
    }

    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    next({ name: "ServerError", errmsg: error });
  }
}

async function logOut(req, res, next) {
  const { id } = req.params;
  const { token } = req.body;
  try {
    let user = await user_service.logOutUser(id, token);
    // let user = await user_service.createUser(userTokens)
    res.status(api_response.code.OK).json(api_response.success(user));
  } catch (error) {
    console.log(error);
    next({ name: "ServerError", errmsg: error });
  }
}

async function logOutAll(req, res, next) {
  const { id } = req.params;
  try {
    let user = await user_service.logOutAllUser(id);
    // let user = await user_service.createUser(userTokens)
    res.status(api_response.code.OK).json(api_response.success(user));
  } catch (error) {
    console.log(error);
    next({ name: "ServerError", errmsg: error });
  }
}

async function editUser(req, res, next) {
  try {
    let { id } = req.params;
    let body = req.body;

    let updatedUser = await user_service.updateUser(id, body);
    res.status(api_response.code.OK).json(api_response.success(updatedUser));
  } catch (error) {
    console.log(error);
    return next({ name: "ServerError", errmsg: error.message });
  }
}

async function getUsersByRole(req, res, next) {
  try {
    let { role } = req.query;
    let users = await user_service.getUsersByRole(role);
    res.status(200).json(users);
  } catch (error) {
    console.log(error);
    return next({ name: "ServerError", errmsg: error.message });
  }
}

async function deleteUser(req, res, next) {
  try {
    let { id } = req.params;
    let deletedUser = await user_service.deleteUser(id);
    res.status(api_response.code.OK).json(api_response.success(deletedUser));
  } catch (error) {
    return next({ name: "ServerError", errmsg: error.message });
  }
}

async function getEmployeeByRegId(req, res, next) {
  try {
    const { reg_id } = req.params;
    let data = await user_service.getEmployeeByRegId(reg_id);
    if (data == null) {
      res.json("");
      next({ name: "ServerError", errmsg: "Object Not Found" });
    }
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: "Object Not Found" });
  }
}

async function signUpUser(req, res, next) {
  try {
    const userReqPayload = req.body;
    let data = await user_service.signUpUser(userReqPayload);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error.stack });
  }
}

async function getUserRequests(req, res, next) {
  try {
    const { active } = req.params;
    let data = await user_service.getUserRequests(active);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error.stack });
  }
}

async function getUserRequestById(req, res, next) {
  try {
    const { id } = req.params;
    let data = await user_service.getUserRequestById(id);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error.stack });
  }
}

async function inactiveUserRequest(req, res, next) {
  try {
    const { id } = req.params;
    let data = await user_service.inactiveUserRequest(id);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error.stack });
  }
}

async function sendEmailTemplate(req, res, next) {
  try {
    const { email } = req.body;
    // let data = await user_service.changePasswordRequest(email);
    await email_handler(email, "data._id", "data.name");
    let response = api_response.success("email-sent");
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error });
  }
}

async function changePassworRequest(req, res, next) {
  try {
    const { email } = req.body;
    let data = await user_service.changePasswordRequest(email);
    await email_handler(data.email, data._id, data.name);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error });
  }
}

async function checkEmailAlreadyRegistered(req, res, next) {
  try {
    const { email } = req.params;
    let data = await user_service.checkEmailAlreadyRegistered(email);
    let response;
    if (data == false) {
      response = api_response.success("Email Not registered !!!");
      res.status(api_response.code.OK).json(response);
    } else {
      response = api_response.success(null);
      res.status(api_response.code.OK).json(response);
    }
  } catch (error) {
    next({ name: "ServerError", errmsg: error });
  }
}

async function changePassworWithVerificationLink(req, res, next) {
  try {
    const { id } = req.params;
    const { new_password } = req.body;
    let data = await user_service.changePassworWithVerificationLink(
      id,
      new_password
    );
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: error });
  }
}

async function changePassword(req, res, next) {
  try {
    const { id } = req.params;
    const { current, new_password } = req.body;
    // console.log({ id, current, new_password });
    let data = await user_service.changePassword(id, current, new_password);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
    // res.end("OK");
  } catch (error) {
    next({ name: "ServerError", errmsg: error });
  }
}

/* async function changePassword(req, res, next) {
  try {
    const { id } = req.params;
    const { email, new_password } = req.body;
    let data = await user_service.changePassword(id, email, new_password);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
    // res.end("OK");
  } catch (error) {
    next({ name: "ServerError", errmsg: error });
  }
} */

module.exports = {
  createUser,
  loginUser,
  silentLoginUser,
  getUsers,
  getUserById,
  uploadPP,
  changePassword,
  editUser,
  logOut,
  logOutAll,
  deleteUser,
  getUsersByRole,
  getEmployeeByRegId,
  signUpUser,
  getUserRequests,
  getUserRequestById,
  inactiveUserRequest,
  sendEmailTemplate,
  changePassword,
  changePassworWithVerificationLink,
  changePassworRequest,
  checkEmailAlreadyRegistered,
};
