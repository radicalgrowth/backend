const api_response = require("../utils/api_response");
const service = require("../services/deviceAllocation.service");
async function createdeviceAllocation(req, res, next) {
  try {
    let payload = req.body;
    let data = await service.createdeviceAllocation(payload);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    next({ name: "ServerError", errmsg: error });
  }
}
async function getAlldeviceAllocations(req, res, next) {
  try {
    let data = await service.getAlldeviceAllocations();
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    return next({ name: "ServerError", errmsg: error.message });
  }
}
async function deletedeviceAllocation(req, res, next) {
  try {
    let { id } = req.params;
    let deletedData = await service.deletedeviceAllocation(id);
    res.status(api_response.code.OK).json(api_response.success(deletedData));
  } catch (error) {
    return next({ name: "ServerError", errmsg: error.message });
  }
}
async function updatedeviceAllocation(req, res, next) {
  try {
    const { id } = req.params;
    const configPayload = req.body;
    let data = await service.updatedeviceAllocation(id, configPayload);
    res.status(api_response.code.OK).json(api_response.success(data));
  } catch (error) {
    console.log(error);
    next(error);
  }
}
async function getdeviceAllocationById(req, res, next) {
  try {
    const { id } = req.params;
    let data = await service.getdeviceAllocationById(id);
    if (data == null) {
      next({ name: "ServerError", errmsg: "Object Not Found" });
    }
    let response = api_response.success(data);
    res.json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: "Object Not Found" });
  }
}
async function getDevicesByUserId(req, res, next) {
  try {
    const { userid } = req.params;
    let data = await service.getDevicesByUserId(userid);
    if (data == null) {
      next({ name: "ServerError", errmsg: "Object Not Found" });
    }
    let response = api_response.success(data);
    res.json(response);
  } catch (error) {
    next({ name: "ServerError", errmsg: "Object Not Found" });
  }
}

module.exports = {
  createdeviceAllocation,
  getAlldeviceAllocations,
  deletedeviceAllocation,
  updatedeviceAllocation,
  getdeviceAllocationById,
  getDevicesByUserId,
};
