const api_response = require('../utils/api_response')
const service = require('../services/chart.service')
async function createChart(req, res, next) {
  try {
    let payload = req.body;
    let data = await service.createChart(payload);
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
     console.log(error)
    next({ name: "ServerError", errmsg: error })
  }
}
async function getAllCharts(req, res, next) {
  try {
    let data = await service.getAllCharts();
    let response = api_response.success(data);
    res.status(api_response.code.OK).json(response);
  } catch (error) {
    console.log(error);
    return next({ name: "ServerError", errmsg: error.message })
  }
}
async function deleteChart(req, res, next) {
  try {
    let { id } = req.params;
    let deletedData = await service.deleteChart(id);
    res.status(api_response.code.OK).json(api_response.success(deletedData))
  } catch (error) {
    return next({ name: "ServerError", errmsg: error.message })
  }
}
async function updateChart(req, res, next) {
  try {
    const { id } = req.params;
    const configPayload = req.body;
    let data = await service.updateChart(id, configPayload);
    res.status(api_response.code.OK).json(api_response.success(data))
  } catch (error) {
    console.log(error);
    next(error)
  }
}
async function getChartById(req, res, next) {
  try {
    const { id } = req.params
    let data = await service.getChartById(id);
    if (data == null) {
      next({ name: "ServerError", errmsg: "Object Not Found" })
    }
    let response = api_response.success(data)
    res.json(response)
  } catch (error) {
    next({ name: "ServerError", errmsg: "Object Not Found" })
  }
}

module.exports = {
  createChart,
  getAllCharts,
  deleteChart,
  updateChart,
  getChartById
}