const Joi = require('@hapi/joi')
const validator = require('express-joi-validation').createValidator({
  passError: true
})
const loggedUserHeaderSchema = Joi.object({
  authorization: Joi.string().required()
})

var loggedUserHeaderValidator = validator.headers(loggedUserHeaderSchema)
module.exports = {
  loggedUserHeaderValidator,
}