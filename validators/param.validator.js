const Joi = require('@hapi/joi')
const validator = require('express-joi-validation').createValidator({
  passError: true
})
const idParamSchema = Joi.object({
  id: Joi.string().required()
})

var idParamValidator = validator.params(idParamSchema)
module.exports = {
  idParamValidator,
}