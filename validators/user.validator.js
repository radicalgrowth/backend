const Joi = require('@hapi/joi')
const validator = require('express-joi-validation').createValidator({
  passError: true
})
const genders = ["male", "female"]
const roles=["admin","doctor","patient"]
const createUserBodySchema = Joi.object({
  regNo: Joi.string(),
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().required().min(4),
  phoneNumber: Joi.string().required(),
  dob: Joi.date().required(),
  gender: Joi.string().equal(...genders).required(),
  address: Joi.string(),
  profile_picture:Joi.string().allow(""),
  description:Joi.string(),
  otherDoctor:Joi.string().allow(""),
  role:Joi.string().equal(...roles).required(),
  assignedTo:Joi.string().allow(null)
})
const editUserBodySchema = Joi.object({
  regNo: Joi.string(),
  name: Joi.string(),
  email: Joi.string().email(),
  password: Joi.string().min(4),
  phoneNumber: Joi.string(),
  gender: Joi.string().equal(...genders),
  address: Joi.string(),
  dob: Joi.date().required(),
  profile_picture:Joi.string().allow(""),
  description:Joi.string(),
  otherDoctor:Joi.string().allow(""),
  assignedTo:Joi.string().allow(null)
  //role:Joi.string().equal(...roles).required()
})
const loginUserBodySchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required()
})
const silentLoginUserBodySchema = Joi.object({
  user_id: Joi.string().required(),
  token: Joi.string().required()
})
const changeUserPasswordBodySchema = Joi.object({
  current: Joi.string().required(),
  new_password: Joi.string().required()
})
const signUpUserBodySchema = Joi.object({
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  phoneNumber: Joi.string().required(),
})

var createUserBodyValidator = validator.body(createUserBodySchema)
var editUserBodyValidator = validator.body(editUserBodySchema)
var loginUserBodyValidator = validator.body(loginUserBodySchema)
var silentLoginUserBodyValidator = validator.body(silentLoginUserBodySchema)
var changeUserPasswordValidator = validator.body(changeUserPasswordBodySchema)
var signUpUserBodyValidator = validator.body(signUpUserBodySchema)
module.exports = {
  createUserBodyValidator,
  loginUserBodyValidator,
  editUserBodyValidator,
  changeUserPasswordValidator,
  silentLoginUserBodyValidator,
  signUpUserBodyValidator
}