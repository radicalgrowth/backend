var constants = require("../utils/constants");
var mongoose = require("mongoose");

var dbURI = constants.DB_HOST;
var dbName = constants.DB_NAME;
console.log(dbName, "database");
var options = {
  dbName: dbName,
  //user: constants.DB_USER,
  //pass: constants.DB_PASS,
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  autoIndex: true, // Don't build indexes
  // reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  //reconnectInterval: 500, // Reconnect every 500ms
  poolSize: 10, // Maintain up to 10 socket connections
  // If not connected, return errors immediately rather than waiting for reconnect
  bufferMaxEntries: 0,
  connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
  socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
  family: 4, // Use IPv4, skip trying IPv6
};
try {
  mongoose.connect(dbURI, options).then(() => {
    console.log("Mongoose default connection open to " + dbURI + "/" + dbName);
    console.log(
      "IOT DATABASE CONNECTED :) | Server Live on port:" + process.env.PORT
    );
  });
} catch (err) {
  console.log(err);
}

/* mongoose.connection.on('connected', function() {
  console.log('Mongoose default connection open to ' + dbURI + "/" + dbName);
  console.log("IOT DATABASE CONNECTED :) | Server Live on port:"+process.env.PORT);
}); */
// If the connection throws an error
mongoose.connection.on("error", function (err) {
  console.log("Mongoose default connection error: " + err);
});

// When the connection is disconnected
mongoose.connection.on("disconnected", function () {
  console.log("Mongoose default connection disconnected");
});

// If the Node process ends, close the Mongoose connection
process.on("SIGINT", function () {
  mongoose.connection.close(function () {
    console.log(
      "Mongoose default connection disconnected through app termination"
    );
    process.exit(0);
  });
});
