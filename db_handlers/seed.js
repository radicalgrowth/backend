var User = require("../models/user.model");
const bcrypt = require("bcryptjs");

var user = {
  // regNo: "0",
  name: "IOT Admin",
  email: "admin@gmail.com",
  password: "1234",
  role: "admin",
  gender: "male",
};
User.countDocuments({}, (err, data) => {
  if (data == 0) {
    hashPasswordAndCreateUser();
  }
});
const hashPasswordAndCreateUser = async function () {
  user.password = await bcrypt.hash(user.password, 8);
  User.create(user, function (e) {
    if (e) {
      throw e;
    } else {
      console.log("Super Admin Created :)");
    }
  });
};
