let httpCodes = {
  /**
   * Success
   */
  OK: 200,
  CREATED: 201,
  NO_DATA: 204,
  /**
   * Redirection Error
   */
  PARAMS_MISSING: 300,
  INVALID_PARAMS: 301,
  /**
   * Client Error
   */
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  METHOD_NOT_ALLOWED: 405,
  CONFLICT: 409,
  /**
   * Server Error
   */
  INTERVAL_SERVER_ERROR: 500,
  INTERVAL_QUEUE_ERROR: 502,
  SERVICE_UNAVAILABLE: 503,
  DB_ERROR: 504,
  DB_CONNECTION_ERROR: 505,
};
let responseMessages = {
  USER_NOT_FOUND: "User not found",
  USER_EXISTS: "User already exists",
  NO_PARENT: "Account is not linked to any company",
  LOGGED_IN: "Successfull Login",

  ACCOUNT_REMOVED: "Account removed successfuly",
  DATA_REMOVED: "Removed Successfully",
  DATA_UPDATED: "Data updated successfuly",
  DATA_CREATED: "Created successfuly",
  DUPLICATION: "Data already exists",

  PASSWORD_INVALID: "Password is incorrect",
  OLD_PASSWORD_INVALID: "Old password is not correct",
  PASSWORD_RESET: "Password reset successfuly",
  EMAIL_INVALID: "Email is not registered",
  EMAIL_ALREADY: "Email is already registered",

  TOKEN_EMPTY: "Token is required",
  TOKEN_INVALID: "Token is invalid or expired",

  EMAIL_ERROR: "Email not sent.Error occured",
  EMAIL_SENT: "Email sent.",

  DATA_NOT_FOUND: "Data not found",

  DB_CONNECTION: "Error establishing connection with database",
  DB_ERROR: "Database error occured",

  UNKNOWN: "Unknown error occured",
  UNAUTHORIZED: "User not authorized /Token Is Required Or May Be Expired",
  SERVER_ERROR: "Internal server error",
};

function successResponse(data) {
  return {
    status: httpCodes.OK,
    message: "OK",
    result: data.length,
    data: data,
  };
}

function errorResponse(message) {
  return {
    status: httpCodes.INTERVAL_SERVER_ERROR,
    data: null,
    message: message,
  };
}
module.exports = {
  success: successResponse,
  error: errorResponse,
  code: httpCodes,
  message: responseMessages,
};
