var multer = require('multer');
var path = require('path');
var fs = require('fs');
var mkdirp = require('mkdirp');
const constants = require('../utils/constants')
const nanoid = require('nanoid');

// var basePath = constants.PUBLIC_DIRECTORY.ORDER_RESULTS;
const FILES_UPLOAD_PATH = path.resolve('./' + constants.UP_PP_PATH);

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    let { id } = req.params;
    console.log('User id', id);
    var uploadPath = FILES_UPLOAD_PATH + '/' + id;
    if (!fs.existsSync(uploadPath)) {
      mkdirp.sync(uploadPath);
    }
    cb(null, uploadPath);
  },
  filename: function(req, file, cb) {
    var checksum = nanoid(5); //=> "58Y2V"
    var tokens = file.originalname.split('.');
    var tempext = '';
    if (tokens.length > 1) {
      name = tokens[0];
      tempext = tokens[1];
      var ext = '.' + tempext;
      cb(null, checksum + ext);
    } else {
      cb(new Error('File is without the extension'));
    }
  }
});
var limits = {
  files: 1, // allow only 1 file per request
  fileSize: process.env.FILE_SIZE * 1024 * 1024 * 8 // 5 MB (max file size)
};
exports.upload = multer({
  storage: storage,
  preservePath: true,
  limits: limits
});