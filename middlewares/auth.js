const jwt = require('jsonwebtoken')
const user_model = require('../models/user.model')

const authUser = async(req, res, next) => {
  try {
    const token = req.header('Authorization').replace('Bearer ', '')
    const data = jwt.verify(token, process.env.JWT_SECRET)
    const user = await user_model.findOne({ _id: data._id, 'tokens.token': token }).select("+password")
    if (!user) {
      throw new Error("No User With This Token");
    }
    req.user = user
    req.token = token
    return next()
  } catch (error) {
    return next({ name: 'ServerError', errmsg: 'Not authorized to access this resource' })
  }
}
const authPatient = async(req, res, next) => {
  header_validator.loggedUserHeaderValidator(req, res, next);
  try {
    const token = req.header('Authorization').replace('Bearer ', '')
    const data = jwt.verify(token, process.env.JWT_SECRET)
    const user = await user_model.findOne({ _id: data._id, 'tokens.token': token })
    if (!user) {
      throw new Error("No User With This Token");
    }
    req.user = user
    req.token = token
    return next()
  } catch (error) {
    return next({ name: 'ServerError', errmsg: 'Not authorized to access this resource' })
  }
}
const authDoctor = async(req, res, next) => {
  const token = req.header('Authorization').replace('Bearer ', '')
  const data = jwt.verify(token, process.env.JWT_SECRET)
  try {
    const user = await user_model.findOne({ _id: data._id, 'tokens.token': token, role })
    if (!user) {
      throw new Error()
    }
    req.user = user
    req.token = token
    next()
  } catch (error) {
    next({ error: 'Not authorized to access this resource' })
  }
}
const authAdmin = async(req, res, next) => {
  const token = req.header('Authorization').replace('Bearer ', '')
  const data = jwt.verify(token, process.env.JWT_SECRET)
  try {
    const user = await user_model.findOne({ _id: data._id, 'tokens.token': token, role })
    if (!user) {
      throw new Error()
    }
    req.user = user
    req.token = token
    next()
  } catch (error) {
    next({ error: 'Not authorized to access this resource' })
  }
}
module.exports = {
  authUser,
  authPatient,
  authDoctor,
  authAdmin
}